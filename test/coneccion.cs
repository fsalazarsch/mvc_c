﻿/*
 * Created by SharpDevelop.
 * User: Felipe
 * Date: 05-11-2017
 * Time: 20:05
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
namespace test
{
	/// <summary>
	/// Description of coneccion.
	/// </summary>
	public class coneccion
	{	
		private MySqlConnection con;
    	private string server;
    	private string db;
    	private string user;
    	private string pwd;
		
    	public coneccion()
		{
    		conectar();
		}
    	
    	public void conectar(){
    		server = "localhost";
    		db = "test";
    		user = "root";
    		pwd = "";
    		String connectionString = "SERVER="+server+";DATABASE="+db+";UID="+user+";PASSWORD="+pwd+";";
    		con = new MySqlConnection(connectionString);
    	}
    	
    	private bool abrirConeccion()
    	{
    		try{
    			con.Open();
    			return true;
    		}
    		catch(MySqlException mysqlex){
    			switch(mysqlex.Number){
    				case 0:
    					Console.WriteLine("No se puede conectar al servidor");
    					break;
    				case 1045:
    					Console.WriteLine("Usuario y/o password invalido");
    					break;
    			}
    			return false;
    		}
    	}
    	
    	private bool cerrarConeccion()
    	{
    		try{
    			con.Close();
    			return true;
    		}
    		catch(MySqlException ex){
    			Console.WriteLine(ex.Message);
    			return false;
    		}
    	}

    	public void Insertar(String nombre, int edad)
    	{
    		String query = "INSERT into usuario VALUES(null, '"+nombre+"', "+edad+")";
    		if(this.abrirConeccion() == true){
    			MySqlCommand cmd = new MySqlCommand();
    			cmd.CommandText = query;
    			cmd.Connection = con;
    			cmd.ExecuteNonQuery();
    			this.cerrarConeccion();
    		}
    	}

	    //Update statement
	    public bool Modificar(int id, String campo, String valor)
    	{
	    	
	    	String query = "UPDATE usuario SET "+campo+"= '"+valor+"' WHERE id= "+id.ToString();
    		if(this.abrirConeccion() == true){
    			MySqlCommand cmd = new MySqlCommand();
    			cmd.CommandText = query;
    			cmd.Connection = con;
    			try {
    			cmd.ExecuteNonQuery();
    			}
    			catch (MySqlException ex){
    				Console.WriteLine(ex.Message);
    				return false;
    			}
    			this.cerrarConeccion();
    			
    		}
	    	return true;
    	}

	    //Delete statement
    	public void Borrar(String nombre)
    	{
    		String query = "DELETE FROM  usuario WHERE nombre LIKE ='"+nombre+"' LIMIT 1";
    		if(this.abrirConeccion() == true){
    			MySqlCommand cmd = new MySqlCommand(query, con);
        		cmd.ExecuteNonQuery();
        		this.cerrarConeccion();
    		}
    	}

	    //Select statement
    	public List <Persona> Listar()
    	{
    		String query = "SELECT * FROM usuario";
    		List<Persona> list = new List<Persona> ();
    		if(this.abrirConeccion() == true){
    			MySqlCommand cmd = new MySqlCommand(query, con);
    			MySqlDataReader datos = cmd.ExecuteReader();
    			
    			while(datos.Read()){
    				list.Add(new Persona((String) datos["nombre"], (int) datos["edad"]));
    			}
    			datos.Close();
    			
    			this.cerrarConeccion();
    			return list;
    		}
    		else{
    			return list;
    		}
    		
    	}

	    //Count statement
    	public int contar()
    	{
			String query = "SELECT COUNT(*) FROM usuario";
			int index = 0;

			if(this.abrirConeccion() == true){
    			MySqlCommand cmd = new MySqlCommand(query, con);
    			index = int.Parse(cmd.ExecuteScalar()+"");
    			this.cerrarConeccion();
    			return index;
    		}
			else{
				return index;
			}
			
    	}

    	
	}
}
