﻿/*
 * Created by SharpDevelop.
 * User: Felipe
 * Date: 05-11-2017
 * Time: 14:19
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace test
{
	/// <summary>
	/// Description of Persona.
	/// </summary>
	public class Persona
	{
		private String nombre;
		private int edad;

		public void setNombre(String nombre){
			this.nombre = nombre;
		}
		
		public void setEdad(int edad){
			this.edad = edad;
		}
		
		public String Nombre {
			get {
				return nombre;
			}
		}
		public int Edad {
			get {
				return edad;
			}
		}

		public Persona(){
		
		}
		public Persona(String nombre, int edad)
		{
			this.nombre = nombre;
			this.edad = edad;
		}
	}
}
