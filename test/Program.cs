﻿/*
 * Created by SharpDevelop.
 * User: Felipe
 * Date: 23-05-2017
 * Time: 16:40
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Linq;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

namespace test
{
	class Program
	{
		
		
		
		public static void Main(string[] args)
		{
			int men = 0;
			// TODO: Implement Functionality Here
			do{
				men = menu();
				acciones_menu(men);
				
			}
			while( men != 5);
				
			
			Console.Write("Ha salido del programa, presione cualuier tecla para contimuar . . . ");
			Console.ReadKey(true);
		}
		
		public static int menu(){
			int opc = 0;
			Console.Clear();
			Console.WriteLine("1) Listar Usuarios");
			Console.WriteLine("2) Insertar Usuario");
			Console.WriteLine("3) Borrar Usuario");
			Console.WriteLine("4) Modificar Usuarios");
			Console.WriteLine("5) Salir");
			Console.Write("Seleccione una opcion:\t");
			opc = int.Parse(Console.ReadLine());
			return opc;
		}
		
		public static void acciones_menu(int opc){
			coneccion c = new coneccion();
			
			switch(opc){
				case 1:
					List<Persona> usuarios = c.Listar();
					Console.WriteLine("|Nombre\t|Edad\t|");
					Console.WriteLine("==========================");
					usuarios.ForEach(Imprimir_usuario);
					Console.WriteLine("Fin lista, presione una tecla para continuar ...");					
					Console.ReadKey();
					break;
				case 2:
					Console.Write("Escriba un Nombre:\t");
					String nom = Console.ReadLine();					
					Console.Write("Escriba la edad:\t");
					int edad = int.Parse(Console.ReadLine());					
					c.Insertar(nom, edad);
					Console.WriteLine("Usuario insertado correctamente");					
					Console.ReadKey();
					break;
				case 3:
					Console.Write("Escriba el nombre a eliminar:\t");
					nom = Console.ReadLine();						
					c.Borrar(nom);
					Console.WriteLine("Usuario eliminado correctamente");					
					Console.ReadKey();
					
					break;
				case 4:
					
					Console.Write("Escriba el id que desea modificar:\t");
					int id = int.Parse(Console.ReadLine());		
					Console.Write("Escriba el campo a modificar:\t");
					nom = Console.ReadLine();					
					Console.Write("Escriba el nuevo valor para el campo:\t");
					String val = Console.ReadLine();					
					
					bool res = c.Modificar(id, nom, val);
					if(res == true)
						Console.WriteLine("Usuario modificado correctamente");
					else
						Console.WriteLine("Ocurrio un error");
					Console.WriteLine("Persone una tecla para continuar");
					Console.ReadKey();
					break;
			}
		}
		
		public static void Imprimir_usuario( Persona p ){
			Console.WriteLine("|"+p.Nombre+"\t|"+p.Edad+"\t|");
			Console.WriteLine("+-----------------------+");
		}
	}
}